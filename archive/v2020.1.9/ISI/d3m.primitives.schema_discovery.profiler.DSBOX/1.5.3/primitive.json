{
    "id": "b2612849-39e4-33ce-bfda-24f3e2cb1e93",
    "version": "1.5.3",
    "name": "DSBox Profiler",
    "python_path": "d3m.primitives.schema_discovery.profiler.DSBOX",
    "primitive_family": "SCHEMA_DISCOVERY",
    "algorithm_types": [
        "DATA_PROFILING"
    ],
    "keywords": [
        "data_profiler"
    ],
    "source": {
        "name": "ISI",
        "contact": "mailto:kyao@isi.edu",
        "uris": [
            "https://github.com/usc-isi-i2/dsbox-primitives"
        ]
    },
    "installation": [
        {
            "type": "PIP",
            "package_uri": "git+https://github.com/usc-isi-i2/dsbox-primitives@390595a708a8702cd6b7b388661127fcf63e4605#egg=dsbox-primitives"
        }
    ],
    "precondition": [],
    "hyperparms_to_tune": [],
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "dsbox.datapreprocessing.cleaner.data_profile.Profiler",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Params": "dsbox.datapreprocessing.cleaner.data_profile.ProfilerParams",
            "Hyperparams": "dsbox.datapreprocessing.cleaner.data_profile.Hyperparams"
        },
        "interfaces_version": "2020.1.9",
        "interfaces": [
            "unsupervised_learning.UnsupervisedLearnerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "split_on_column_with_avg_len": {
                "type": "d3m.metadata.hyperparams.Uniform",
                "default": 30,
                "structural_type": "float",
                "semantic_types": [
                    "http://schema.org/Integer",
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Threshold of avg column length for splitting punctuation or alphanumeric",
                "lower": 10,
                "upper": 100,
                "lower_inclusive": true,
                "upper_inclusive": true
            },
            "metafeatures": {
                "type": "d3m.metadata.hyperparams.Set",
                "default": [
                    "ratio_of_values_containing_numeric_char",
                    "ratio_of_numeric_values",
                    "number_of_outlier_numeric_values",
                    "num_filename",
                    "number_of_tokens_containing_numeric_char",
                    "semantic_types"
                ],
                "structural_type": "typing.Sequence[str]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/MetafeatureParameter"
                ],
                "description": "Compute metadata descriptions of the dataset",
                "elements": {
                    "type": "d3m.metadata.hyperparams.Enumeration",
                    "default": "ratio_of_values_containing_numeric_char",
                    "structural_type": "str",
                    "semantic_types": [
                        "https://metadata.datadrivendiscovery.org/types/MetafeatureParameter"
                    ],
                    "values": [
                        "ratio_of_values_containing_numeric_char",
                        "ratio_of_numeric_values",
                        "number_of_outlier_numeric_values",
                        "num_filename",
                        "number_of_tokens_containing_numeric_char",
                        "number_of_numeric_values_equal_-1",
                        "most_common_numeric_tokens",
                        "most_common_tokens",
                        "ratio_of_distinct_tokens",
                        "number_of_missing_values",
                        "number_of_distinct_tokens_split_by_punctuation",
                        "number_of_distinct_tokens",
                        "ratio_of_missing_values",
                        "semantic_types",
                        "number_of_numeric_values_equal_0",
                        "number_of_positive_numeric_values",
                        "most_common_alphanumeric_tokens",
                        "numeric_char_density",
                        "ratio_of_distinct_values",
                        "number_of_negative_numeric_values",
                        "target_values",
                        "ratio_of_tokens_split_by_punctuation_containing_numeric_char",
                        "ratio_of_values_with_leading_spaces",
                        "number_of_values_with_trailing_spaces",
                        "ratio_of_values_with_trailing_spaces",
                        "number_of_numeric_values_equal_1",
                        "natural_language_of_feature",
                        "most_common_punctuations",
                        "spearman_correlation_of_features",
                        "number_of_values_with_leading_spaces",
                        "ratio_of_tokens_containing_numeric_char",
                        "number_of_tokens_split_by_punctuation_containing_numeric_char",
                        "number_of_numeric_values",
                        "ratio_of_distinct_tokens_split_by_punctuation",
                        "number_of_values_containing_numeric_char",
                        "most_common_tokens_split_by_punctuation",
                        "number_of_distinct_values",
                        "pearson_correlation_of_features"
                    ]
                },
                "is_configuration": false,
                "min_size": 1,
                "max_size": 38
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "dsbox.datapreprocessing.cleaner.data_profile.Hyperparams",
                "kind": "RUNTIME"
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "params": {
                "type": "dsbox.datapreprocessing.cleaner.data_profile.ProfilerParams",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {},
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams"
                ],
                "returns": "NoneType"
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "Fits primitive using inputs and outputs (if any) using currently set training data.\n\nThe returned value should be a ``CallResult`` object with ``value`` set to ``None``.\n\nIf ``fit`` has already been called in the past on different training data,\nthis method fits it **again from scratch** using currently set training data.\n\nOn the other hand, caller can call ``fit`` multiple times on the same training data\nto continue fitting.\n\nIf ``fit`` fully fits using provided training data, there is no point in making further\ncalls to this method with same training data, and in fact further calls can be noops,\nor a primitive can decide to fully refit from scratch.\n\nIn the case fitting can continue with same training data (even if it is maybe not reasonable,\nbecause the internal metric primitive is using looks like fitting will be degrading), if ``fit``\nis called again (without setting training data), the primitive has to continue fitting.\n\nCaller can provide ``timeout`` information to guide the length of the fitting process.\nIdeally, a primitive should adapt its fitting process to try to do the best fitting possible\ninside the time allocated. If this is not possible and the primitive reaches the timeout\nbefore fitting, it should raise a ``TimeoutError`` exception to signal that fitting was\nunsuccessful in the given time. The state of the primitive after the exception should be\nas the method call has never happened and primitive should continue to operate normally.\nThe purpose of ``timeout`` is to give opportunity to a primitive to cleanly manage\nits state instead of interrupting execution from outside. Maintaining stable internal state\nshould have precedence over respecting the ``timeout`` (caller can terminate the misbehaving\nprimitive from outside anyway). If a longer ``timeout`` would produce different fitting,\nthen ``CallResult``'s ``has_finished`` should be set to ``False``.\n\nSome primitives have internal fitting iterations (for example, epochs). For those, caller\ncan provide how many of primitive's internal iterations should a primitive do before returning.\nPrimitives should make iterations as small as reasonable. If ``iterations`` is ``None``,\nthen there is no limit on how many iterations the primitive should do and primitive should\nchoose the best amount of iterations on its own (potentially controlled through\nhyper-parameters). If ``iterations`` is a number, a primitive has to do those number of\niterations (even if not reasonable), if possible. ``timeout`` should still be respected\nand potentially less iterations can be done because of that. Primitives with internal\niterations should make ``CallResult`` contain correct values.\n\nFor primitives which do not have internal iterations, any value of ``iterations``\nmeans that they should fit fully, respecting only ``timeout``.\n\nParameters\n----------\ntimeout : float\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[None]\n    A ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to ``set_training_data`` and all produce methods.\ntimeout : float\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "dsbox.datapreprocessing.cleaner.data_profile.ProfilerParams",
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nParams\n    An instance of parameters."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to all produce methods.\ntimeout : float\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "generate features for the input.\nInput:\n    typing.Union[container.Dataset, container.DataFrame, container.ndarray, container.matrix, container.List]\nOutput:\n    typing.Union[container.Dataset, container.DataFrame, container.ndarray, container.matrix, container.List]\n\nParameters\n----------\ninputs : Inputs\n    The inputs of shape [num_inputs, ...].\ntimeout : float\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[Outputs]\n    The outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams : Params\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [
                    "inputs"
                ],
                "returns": "NoneType",
                "description": "Sets training data of this primitive.\n\nParameters\n----------\ninputs : Inputs\n    The inputs."
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        },
        "params": {
            "mapping": "str"
        }
    },
    "structural_type": "dsbox.datapreprocessing.cleaner.data_profile.Profiler",
    "description": "Generate a profile of the given dataset. The profiler is capable of detecting if column values consists of compound\nvalues, date values, phone number values, alphanumeric token values and categorical values.\n\nAttributes\n----------\nmetadata : PrimitiveMetadata\n    Primitive's metadata. Available as a class attribute.\nlogger : Logger\n    Primitive's logger. Available as a class attribute.\nhyperparams : Hyperparams\n    Hyperparams passed to the constructor.\nrandom_seed : int\n    Random seed passed to the constructor.\ndocker_containers : Dict[str, DockerContainer]\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes : Dict[str, str]\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory : str\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "digest": "cf5b31c698c83f5dc2adf4a4d3ebbf483c38418ecadd5d94c435b10654b13f91"
}
