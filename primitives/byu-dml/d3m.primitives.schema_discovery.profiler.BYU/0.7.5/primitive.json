{
    "algorithm_types": [
        "DATA_PROFILING"
    ],
    "description": "A primitive which determines missing semantic types for columns and adds\nthem automatically. It uses a set of hard-coded rules/heuristics to determine\nsemantic types. Feel free to propose improvements.\n\nBesides determining column types it also determines some column roles.\n\nSome rules are intuitive and expected, but there are also few special behaviors\n(if not disabled by not providing a corresponding semantic type in\n``detect_semantic_types``):\n\n* If a column does not have any semantic types,\n  ``https://metadata.datadrivendiscovery.org/types/UnknownType`` semantic type\n  is first set for the column. If any other semantic type is set later on as\n  part of logic of this primitive, the\n  ``https://metadata.datadrivendiscovery.org/types/UnknownType`` is removed\n  (including if the column originally came with this semantic type).\n* If a column has ``https://metadata.datadrivendiscovery.org/types/SuggestedTarget``\n  semantic type and no other column (even those not otherwise operated on by\n  the primitive) has a semantic type\n  ``https://metadata.datadrivendiscovery.org/types/TrueTarget`` is set on\n  the column. This allows operation on data without a problem description.\n  This is only for the first such column.\n* All other columns which are missing semantic types initially we set as\n  ``https://metadata.datadrivendiscovery.org/types/Attribute``.\n* Any column with ``http://schema.org/DateTime`` semantic type is also set\n  as ``https://metadata.datadrivendiscovery.org/types/Time`` semantic type.\n* ``https://metadata.datadrivendiscovery.org/types/PrimaryKey`` or\n  ``https://metadata.datadrivendiscovery.org/types/PrimaryMultiKey`` is set only\n  if no other column (even those not otherwise operated on by\n  the primitive) is a primary key, and set based on the column name: only\n  when it is ``d3mIndex``.\n\nAttributes\n----------\nmetadata:\n    Primitive's metadata. Available as a class attribute.\nlogger:\n    Primitive's logger. Available as a class attribute.\nhyperparams:\n    Hyperparams passed to the constructor.\nrandom_seed:\n    Random seed passed to the constructor.\ndocker_containers:\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes:\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory:\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "digest": "743283ab80cd7def170135ccb5f81d28ea14b2eae955caf751216f3a9c0e4f94",
    "id": "af214333-e67b-4e59-a49b-b16f5501a925",
    "installation": [
        {
            "package": "byudml",
            "type": "PIP",
            "version": "0.7.5"
        },
        {
            "file_digest": "9d3dfdf353743741a4be36250868636d377b0dcc16067bfcdfc192fe7239d7c8",
            "file_uri": "https://public.ukp.informatik.tu-darmstadt.de/reimers/sentence-transformers/v0.2/distilbert-base-nli-stsb-mean-tokens.zip",
            "key": "distilbert-base-nli-stsb-mean-tokens.zip",
            "type": "FILE"
        }
    ],
    "name": "Semantic Profiler",
    "original_python_path": "byudml.profiler.profiler_primitive.SemanticProfilerPrimitive",
    "primitive_code": {
        "arguments": {
            "hyperparams": {
                "kind": "RUNTIME",
                "type": "byudml.profiler.profiler_primitive.Hyperparams"
            },
            "inputs": {
                "kind": "PIPELINE",
                "type": "d3m.container.pandas.DataFrame"
            },
            "iterations": {
                "default": null,
                "kind": "RUNTIME",
                "type": "typing.Union[NoneType, int]"
            },
            "params": {
                "kind": "RUNTIME",
                "type": "byudml.profiler.profiler_primitive.Params"
            },
            "produce_methods": {
                "kind": "RUNTIME",
                "type": "typing.Sequence[str]"
            },
            "timeout": {
                "default": null,
                "kind": "RUNTIME",
                "type": "typing.Union[NoneType, float]"
            },
            "volumes": {
                "default": null,
                "kind": "RUNTIME",
                "type": "typing.Union[NoneType, typing.Dict[str, str]]"
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "class_methods": {},
        "class_type_arguments": {
            "Hyperparams": "byudml.profiler.profiler_primitive.Hyperparams",
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Params": "byudml.profiler.profiler_primitive.Params"
        },
        "hyperparams": {
            "add_index_columns": {
                "default": true,
                "description": "Also include primary index columns if input data has them. Applicable only if \"return_result\" is set to \"new\".",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "structural_type": "bool",
                "type": "d3m.metadata.hyperparams.UniformBool"
            },
            "categorical_max_absolute_distinct_values": {
                "configuration": {
                    "limit": {
                        "default": 50,
                        "lower": 1,
                        "lower_inclusive": true,
                        "semantic_types": [],
                        "structural_type": "int",
                        "type": "d3m.metadata.hyperparams.Bounded",
                        "upper": null,
                        "upper_inclusive": false
                    },
                    "unlimited": {
                        "default": null,
                        "description": "No absolute limit on distinct values.",
                        "semantic_types": [],
                        "structural_type": "NoneType",
                        "type": "d3m.metadata.hyperparams.Hyperparameter"
                    }
                },
                "default": 50,
                "description": "The maximum absolute number of distinct values (all missing values as counted as one distinct value) for a column to be considered categorical.",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "structural_type": "typing.Union[NoneType, int]",
                "type": "d3m.metadata.hyperparams.Union"
            },
            "categorical_max_ratio_distinct_values": {
                "default": 0.05,
                "description": "The maximum ratio of distinct values (all missing values as counted as one distinct value) vs. number of rows for a column to be considered categorical.",
                "lower": 0,
                "lower_inclusive": true,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "structural_type": "float",
                "type": "d3m.metadata.hyperparams.Bounded",
                "upper": 1,
                "upper_inclusive": true
            },
            "detect_semantic_types": {
                "default": [
                    "http://schema.org/Boolean",
                    "https://metadata.datadrivendiscovery.org/types/CategoricalData",
                    "http://schema.org/Integer",
                    "http://schema.org/Float",
                    "http://schema.org/Text",
                    "https://metadata.datadrivendiscovery.org/types/FloatVector",
                    "http://schema.org/DateTime",
                    "https://metadata.datadrivendiscovery.org/types/UniqueKey",
                    "https://metadata.datadrivendiscovery.org/types/Attribute",
                    "https://metadata.datadrivendiscovery.org/types/Time",
                    "https://metadata.datadrivendiscovery.org/types/TrueTarget",
                    "https://metadata.datadrivendiscovery.org/types/UnknownType",
                    "https://metadata.datadrivendiscovery.org/types/PrimaryKey",
                    "https://metadata.datadrivendiscovery.org/types/PrimaryMultiKey"
                ],
                "description": "A set of semantic types to detect and set. One can provide a subset of supported semantic types to limit what the primitive detects.",
                "elements": {
                    "default": "http://schema.org/Boolean",
                    "semantic_types": [],
                    "structural_type": "str",
                    "type": "d3m.metadata.hyperparams.Enumeration",
                    "values": [
                        "http://schema.org/Boolean",
                        "https://metadata.datadrivendiscovery.org/types/CategoricalData",
                        "http://schema.org/Integer",
                        "http://schema.org/Float",
                        "http://schema.org/Text",
                        "https://metadata.datadrivendiscovery.org/types/FloatVector",
                        "http://schema.org/DateTime",
                        "https://metadata.datadrivendiscovery.org/types/UniqueKey",
                        "https://metadata.datadrivendiscovery.org/types/Attribute",
                        "https://metadata.datadrivendiscovery.org/types/Time",
                        "https://metadata.datadrivendiscovery.org/types/TrueTarget",
                        "https://metadata.datadrivendiscovery.org/types/UnknownType",
                        "https://metadata.datadrivendiscovery.org/types/PrimaryKey",
                        "https://metadata.datadrivendiscovery.org/types/PrimaryMultiKey"
                    ]
                },
                "is_configuration": false,
                "min_size": 0,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "structural_type": "typing.Sequence[str]",
                "type": "d3m.metadata.hyperparams.Set"
            },
            "exclude_columns": {
                "default": [],
                "description": "A set of column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
                "elements": {
                    "default": -1,
                    "semantic_types": [],
                    "structural_type": "int",
                    "type": "d3m.metadata.hyperparams.Hyperparameter"
                },
                "is_configuration": false,
                "min_size": 0,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "structural_type": "typing.Sequence[int]",
                "type": "d3m.metadata.hyperparams.Set"
            },
            "nan_values": {
                "default": [
                    "",
                    "#N/A",
                    "#N/A N/A",
                    "#NA",
                    "-1.#IND",
                    "-1.#QNAN",
                    "-NaN",
                    "-nan",
                    "1.#IND",
                    "1.#QNAN",
                    "<NA>",
                    "N/A",
                    "NA",
                    "NULL",
                    "NaN",
                    "n/a",
                    "nan",
                    "null"
                ],
                "description": "A set of strings to recognize as NaNs when detecting a float column.",
                "elements": {
                    "default": "",
                    "semantic_types": [],
                    "structural_type": "str",
                    "type": "d3m.metadata.hyperparams.Hyperparameter"
                },
                "is_configuration": false,
                "min_size": 0,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "structural_type": "typing.Sequence[str]",
                "type": "d3m.metadata.hyperparams.Set"
            },
            "remove_unknown_type": {
                "default": true,
                "description": "Remove \"https://metadata.datadrivendiscovery.org/types/UnknownType\" semantic type from columns on which the primitive has detected other semantic types.",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "structural_type": "bool",
                "type": "d3m.metadata.hyperparams.UniformBool"
            },
            "replace_index_columns": {
                "default": true,
                "description": "Replace primary index columns even if otherwise appending columns. Applicable only if \"return_result\" is set to \"append\".",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "structural_type": "bool",
                "type": "d3m.metadata.hyperparams.UniformBool"
            },
            "return_result": {
                "default": "replace",
                "description": "Should detected columns be appended, should they replace original columns, or should only detected columns be returned?",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "structural_type": "str",
                "type": "d3m.metadata.hyperparams.Enumeration",
                "values": [
                    "append",
                    "replace",
                    "new"
                ]
            },
            "text_min_ratio_values_with_whitespace": {
                "default": 0.5,
                "description": "The minimum ratio of values with any whitespace (after first stripping) vs. number of rows for a column to be considered a text column.",
                "lower": 0,
                "lower_inclusive": true,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "structural_type": "float",
                "type": "d3m.metadata.hyperparams.Bounded",
                "upper": 1,
                "upper_inclusive": true
            },
            "use_columns": {
                "default": [],
                "description": "A set of column indices to force primitive to operate on. If any specified column cannot be detected, it is skipped.",
                "elements": {
                    "default": -1,
                    "semantic_types": [],
                    "structural_type": "int",
                    "type": "d3m.metadata.hyperparams.Hyperparameter"
                },
                "is_configuration": false,
                "min_size": 0,
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "structural_type": "typing.Sequence[int]",
                "type": "d3m.metadata.hyperparams.Set"
            }
        },
        "instance_attributes": {
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "temporary_directory": "typing.Union[NoneType, str]",
            "volumes": "typing.Dict[str, str]"
        },
        "instance_methods": {
            "__init__": {
                "arguments": [
                    "hyperparams",
                    "volumes"
                ],
                "kind": "OTHER",
                "returns": "NoneType"
            },
            "fit": {
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "description": "Fits primitive using inputs and outputs (if any) using currently set training data.\n\nThe returned value should be a ``CallResult`` object with ``value`` set to ``None``.\n\nIf ``fit`` has already been called in the past on different training data,\nthis method fits it **again from scratch** using currently set training data.\n\nOn the other hand, caller can call ``fit`` multiple times on the same training data\nto continue fitting.\n\nIf ``fit`` fully fits using provided training data, there is no point in making further\ncalls to this method with same training data, and in fact further calls can be noops,\nor a primitive can decide to fully refit from scratch.\n\nIn the case fitting can continue with same training data (even if it is maybe not reasonable,\nbecause the internal metric primitive is using looks like fitting will be degrading), if ``fit``\nis called again (without setting training data), the primitive has to continue fitting.\n\nCaller can provide ``timeout`` information to guide the length of the fitting process.\nIdeally, a primitive should adapt its fitting process to try to do the best fitting possible\ninside the time allocated. If this is not possible and the primitive reaches the timeout\nbefore fitting, it should raise a ``TimeoutError`` exception to signal that fitting was\nunsuccessful in the given time. The state of the primitive after the exception should be\nas the method call has never happened and primitive should continue to operate normally.\nThe purpose of ``timeout`` is to give opportunity to a primitive to cleanly manage\nits state instead of interrupting execution from outside. Maintaining stable internal state\nshould have precedence over respecting the ``timeout`` (caller can terminate the misbehaving\nprimitive from outside anyway). If a longer ``timeout`` would produce different fitting,\nthen ``CallResult``'s ``has_finished`` should be set to ``False``.\n\nSome primitives have internal fitting iterations (for example, epochs). For those, caller\ncan provide how many of primitive's internal iterations should a primitive do before returning.\nPrimitives should make iterations as small as reasonable. If ``iterations`` is ``None``,\nthen there is no limit on how many iterations the primitive should do and primitive should\nchoose the best amount of iterations on its own (potentially controlled through\nhyper-parameters). If ``iterations`` is a number, a primitive has to do those number of\niterations (even if not reasonable), if possible. ``timeout`` should still be respected\nand potentially less iterations can be done because of that. Primitives with internal\niterations should make ``CallResult`` contain correct values.\n\nFor primitives which do not have internal iterations, any value of ``iterations``\nmeans that they should fit fully, respecting only ``timeout``.\n\nParameters\n----------\ntimeout:\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA ``CallResult`` with ``None`` value.",
                "kind": "OTHER",
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]"
            },
            "fit_multi_produce": {
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to ``set_training_data`` and all produce methods.\ntimeout:\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``.",
                "kind": "OTHER",
                "returns": "d3m.primitive_interfaces.base.MultiCallResult"
            },
            "get_params": {
                "arguments": [],
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nAn instance of parameters.",
                "kind": "OTHER",
                "returns": "byudml.profiler.profiler_primitive.Params"
            },
            "multi_produce": {
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to all produce methods.\ntimeout:\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``.",
                "kind": "OTHER",
                "returns": "d3m.primitive_interfaces.base.MultiCallResult"
            },
            "produce": {
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "description": "Produce primitive's best choice of the output for each of the inputs.\n\nThe output value should be wrapped inside ``CallResult`` object before returning.\n\nIn many cases producing an output is a quick operation in comparison with ``fit``, but not\nall cases are like that. For example, a primitive can start a potentially long optimization\nprocess to compute outputs. ``timeout`` and ``iterations`` can serve as a way for a caller\nto guide the length of this process.\n\nIdeally, a primitive should adapt its call to try to produce the best outputs possible\ninside the time allocated. If this is not possible and the primitive reaches the timeout\nbefore producing outputs, it should raise a ``TimeoutError`` exception to signal that the\ncall was unsuccessful in the given time. The state of the primitive after the exception\nshould be as the method call has never happened and primitive should continue to operate\nnormally. The purpose of ``timeout`` is to give opportunity to a primitive to cleanly\nmanage its state instead of interrupting execution from outside. Maintaining stable internal\nstate should have precedence over respecting the ``timeout`` (caller can terminate the\nmisbehaving primitive from outside anyway). If a longer ``timeout`` would produce\ndifferent outputs, then ``CallResult``'s ``has_finished`` should be set to ``False``.\n\nSome primitives have internal iterations (for example, optimization iterations).\nFor those, caller can provide how many of primitive's internal iterations\nshould a primitive do before returning outputs. Primitives should make iterations as\nsmall as reasonable. If ``iterations`` is ``None``, then there is no limit on\nhow many iterations the primitive should do and primitive should choose the best amount\nof iterations on its own (potentially controlled through hyper-parameters).\nIf ``iterations`` is a number, a primitive has to do those number of iterations,\nif possible. ``timeout`` should still be respected and potentially less iterations\ncan be done because of that. Primitives with internal iterations should make\n``CallResult`` contain correct values.\n\nFor primitives which do not have internal iterations, any value of ``iterations``\nmeans that they should run fully, respecting only ``timeout``.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\ninputs:\n    The inputs of shape [num_inputs, ...].\ntimeout:\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nThe outputs of shape [num_inputs, ...] wrapped inside ``CallResult``.",
                "inputs_across_samples": [],
                "kind": "PRODUCE",
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false
            },
            "set_params": {
                "arguments": [
                    "params"
                ],
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams:\n    An instance of parameters.",
                "kind": "OTHER",
                "returns": "NoneType"
            },
            "set_training_data": {
                "arguments": [
                    "inputs"
                ],
                "description": "Sets training data of this primitive.\n\nParameters\n----------\ninputs:\n    The inputs.",
                "kind": "OTHER",
                "returns": "NoneType"
            }
        },
        "interfaces": [
            "unsupervised_learning.UnsupervisedLearnerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "interfaces_version": "2020.5.18",
        "params": {
            "add_semantic_types": "typing.Union[NoneType, typing.List[typing.List[str]]]",
            "remove_semantic_types": "typing.Union[NoneType, typing.List[typing.List[str]]]"
        }
    },
    "primitive_family": "SCHEMA_DISCOVERY",
    "python_path": "d3m.primitives.schema_discovery.profiler.BYU",
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "source": {
        "contact": "mailto:bjschoenfeld@gmail.com",
        "name": "byu-dml",
        "uris": [
            "https://github.com/byu-dml/d3m-primitives"
        ]
    },
    "structural_type": "byudml.profiler.profiler_primitive.SemanticProfilerPrimitive",
    "version": "0.7.5"
}